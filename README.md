Ridiculously simple script to load a wikipedia dump into a postgres database

## Usage
* update connection info in main.rb
* `bzcat wiki-dump.xml.bz2 | ruby main.rb`
* wiki all the (english) things!

## Note
Postgres has some really nifty [xml functionality](http://www.postgresql.org/docs/9.3/static/functions-xml.html#FUNCTIONS-XML-PROCESSING), so I elected to store the xml for the pages, since it contains valuable metadata, and leave any stripping functionality up to whatever application utilizing this db.

## Other note
I might be an idiot and there may well be a postgres dump already available, but I didn't see it on their main download page, and this was a fun exercise :)
